﻿namespace Summoner
{
    using System;
    using System.Collections.Generic;

    public class PoolBase<T> : IDisposable
    {
        private Queue<T> _pool;
        private T _prefab;

        public virtual void Init(T prefab)
        {
            this._pool = new Queue<T>();
            this._prefab = prefab;
        }

        public virtual T Instantiate(bool poolOnly)
        {
            return default(T);
        }

        public virtual void Recycle(T obj)
        {
            this.Pool.Enqueue(obj);
        }

        public virtual void RemoveOne()
        {
            this.Pool.Dequeue();
        }

        public virtual void RemoveAllPooled()
        {
            for (int i = 0; i < this.Pool.Count; i++)
            {
                this.Pool.Dequeue();
            }
            GC.Collect();
        }

        public virtual void End()
        {
            this.RemoveAllPooled();
        }

        public virtual Queue<T> Pool { get { return this._pool; } }

        public virtual int Count { get { return this.Pool.Count; } }

        public virtual T Prefab
        {
            get { return this._prefab; }
            set { this._prefab = value; }
        }

        #region IDisposable
        private bool disposed = false; // To detect redundant calls

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) { return; }
            if (disposing)
            {
                // Free any other managed objects here.
                this.End();
            }
            // Free any unmanaged objects here.
            disposed = true;
        }
        #endregion
    }
}