﻿namespace Summoner
{
    using UnityEngine;
    using System.Collections.Generic;
    using System;
    using System.IO;
    public class PartialLoadMgr : MonoBehaviour
    {
        private static PartialLoadMgr _instance = null;
        private Queue<PartialLoad> _partialqueue = null;
        private PartialLoad _curpartial = null;
        private bool _isRunning = false;
        public Action onCompleted = null;

        public PartialLoadMgr()
        {
            this._partialqueue = new Queue<PartialLoad>();
        }

        public void PartialLoadMission(string assetName, string ext, Action callback)
        {
            this.onCompleted = callback;
            string remoteFile = AssetPath.UpdatePath + "/" + assetName.ToLower() + "." + ext;
            string cacheFile = AssetPath.TemporaryCachePath + "/" + assetName.ToLower() + "." + ext;
            string localFile = AssetPath.DocumentsPath + "/" + assetName.ToLower() + "." + ext;
            this._partialqueue.Enqueue(new PartialLoad(remoteFile, cacheFile, localFile, string.Empty, this.OnLoaded));
            this.CheckMissionQueue();
        }

        public void PartialLoadMissions(List<AssetInfo> assets, Action callback)
        {
            this.onCompleted = callback;
            foreach(AssetInfo  asset in assets)
            {
                string ext = this.GetFileExtName(asset);
                string assetName = asset.assetName == AssetPath.StreamFolderName ? AssetPath.StreamFolderName : asset.assetName.ToLower();
                string remoteFile = AssetPath.NetPath + "/" + assetName + ext;
                string cacheFile = AssetPath.TemporaryCachePath + "/" + assetName + ext;
                string localFile = AssetPath.DocumentsPath + "/" + assetName + ext;
                this._partialqueue.Enqueue(new PartialLoad(remoteFile, cacheFile, localFile, asset.Md5, this.OnLoaded));
                if (asset.type != "UnityEngine.TextAsset")
                {
                    string remoteManifestFile = remoteFile + ".manifest";
                    string cacheManifestFile = cacheFile + ".manifest";
                    string localManifestFile = localFile + ".manifest";
                    this._partialqueue.Enqueue(new PartialLoad(remoteManifestFile, cacheManifestFile, localManifestFile, asset.Md5, this.OnLoaded));
                }
            }
            this.CheckMissionQueue();
        }

        private string GetFileExtName(AssetInfo asset)
        {
            if(asset.assetName == AssetPath.StreamFolderName)
            {
                return "";
            }
            if(asset.type == "UnityEngine.TextAsset")
            {
                if (asset.pathInEditor.Contains("."))
                {
                    string[] strs = asset.pathInEditor.Split('.');
                    return "." + strs[strs.Length - 1];
                }
            }
            return ".unity3d";
        }

        private void OnLoaded()
        {
            this._isRunning = false;
            if (this._partialqueue.Count > 0)
            {
                this.CheckMissionQueue();
            }
            else
            {
                if(this.onCompleted != null)
                {
                    this.onCompleted();
                }
            }
        }

        private void CheckMissionQueue()
        {
            if(!this._isRunning)
            {
                this._isRunning = true;
                this._curpartial = this._partialqueue.Dequeue();
                StartCoroutine(this._curpartial.Download());
            }
        }

        public static PartialLoadMgr instance
        {
            get
            {
                if (_instance == null) { _instance = FindObjectOfType<PartialLoadMgr>(); }
                if (_instance == null) { _instance = new GameObject("PartialLoadMgr").AddComponent<PartialLoadMgr>(); }
                return _instance;
            }
        }
    }
}