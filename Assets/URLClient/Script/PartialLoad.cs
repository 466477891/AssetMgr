﻿namespace Summoner
{
    using UnityEngine;
    using System.Net;
    using System.IO;
    using System.Collections;
    using System;
#if UNITY_STANDALONE_OSX || UNITY_IOS || UNITY_ANDROID
    using URLClient;
#endif

    public class PartialLoad
    {
        private bool _isRunning = false;
        private string _assetName;
        private string _remoteFile;
        private string _cacheFile;
        private string _localFile;
        private string _md5;
        private DateTime _localModifyDate;
        private DateTime _remoteModifyDate;
        private long _remoteFileSize = 0;
        private bool _hadError = false;
#if UNITY_WEBPLAYER || UNITY_WEBGL
	    private bool _isDone = false;
#else
        private HttpWebResponse _AsynchResponse = null;
#endif
        public Action onLoadCompleted = null;

        /// <summary>
        /// Load asset to documents folder with asset name and extension name
        /// </summary>
        /// <param name="name"></param>
        public PartialLoad(string remoteFile, string cacheFile, string localFile, string md5, Action callback)
        {
            this._assetName = this.GetAssetName(localFile);
            this._remoteFile = remoteFile;
            this._cacheFile = cacheFile;
            this._localFile = localFile;
            this._md5 = md5;
            this.onLoadCompleted = callback;
            Debug.Log("[ResumableDownload] Creating download for: " + this._remoteFile);
            if(File.Exists(this._localFile))
            {
                this._localModifyDate = File.GetLastWriteTime(this._localFile);
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this._remoteFile);
            // Only the header info, not full file!
            request.Method = "HEAD"; 
            // Get response will throw WebException if file is not found
            HttpWebResponse resp = null;
            try
            {
                resp = (HttpWebResponse)request.GetResponse();
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("<color=red>ERROR: " + this._remoteFile + "</color>");
                Debug.LogWarning("ERROR: " + e);
                this._hadError = true;
                return;
            }
            this._remoteFileSize = resp.ContentLength;
            this._remoteModifyDate = resp.LastModified;
            resp.Close();
        }

        private string GetAssetName(string path)
        {
            path.Replace("\\", "/");
            string[] arr = path.Split('/');
            return arr[arr.Length - 1].Split('.')[0];
        }

        /// <summary>
        /// check file is outdate or not
        /// </summary>
        private bool IsOutdated
        {
            get
            {
                if(File.Exists(this._localFile))
                {
                    return this._remoteModifyDate > this._localModifyDate;
                }
                return true;
            }
        }

        // It's the callers responsibility to start this as a coroutine!
        public IEnumerator Download()
        {
            if (this._hadError) { yield break; }
            while (this._isRunning) { yield return null; }
            this._isRunning = true;
            long localFileSize = (File.Exists(this._localFile)) ? (new FileInfo(this._localFile)).Length : 0;
            long cacheFileSize = (File.Exists(this._cacheFile)) ? (new FileInfo(this._cacheFile)).Length : 0;

            if (localFileSize == this._remoteFileSize && !IsOutdated)
            {
                Debug.Log("File already cached, not downloading: " + this._localFile);
                this._isRunning = false;
                if (File.Exists(this._cacheFile)) { File.Delete(this._cacheFile); }
                while (File.Exists(this._cacheFile)) { yield return null; }
                if (this.onLoadCompleted != null)
                {
                    this.onLoadCompleted();
                }
                yield break; // We already have the file, early out
            }

#if UNITY_WEBPLAYER || UNITY_WEBGL
		    using (WebClient client = new WebClient()) 
            {
			    client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompleted);
			    client.DownloadFileAsync(new Uri(this._remoteFile), this._cacheFile);
		    }
		    while (!this._isDone){ yield return null; }
#elif UNITY_STANDALONE_OSX || UNITY_IOS || UNITY_ANDROID
            Debug.Log("Downloading: " + _remoteFile);
            using (HTTPClient client = new HTTPClient(this._remoteFile, this._cacheFile))
            {
                while (!client.IsDone) { yield return null; }
                if (client.Error != null || client.Response == null)
                {
                    Debug.LogError("ERROR: Integrity check aborted");
                    Debug.LogError(client.Error.ToString());
                }
            }
#else
            int bufferSize = 1024 * 1000;
            Debug.Log("Downloading: " + _remoteFile);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(this._cacheFile);
            request.Timeout = 30000;
            request.AddRange((int)cacheFileSize, (int)_remoteFileSize - 1);
            request.Method = WebRequestMethods.Http.Post;
            request.BeginGetResponse(AsynchCallback, request);
            while (this._AsynchResponse == null) { yield return null; }

            using (Stream inStream = this._AsynchResponse.GetResponseStream())
            {
                using (FileStream outStream = new FileStream(this._cacheFile, (cacheFileSize > 0) ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    int count = 0;
                    byte[] buff = new byte[bufferSize];
                    while ((count = inStream.Read(buff, 0, bufferSize)) > 0)
                    {
                        outStream.Write(buff, 0, count);
                        outStream.Flush();
                        yield return null;
                    }
                    outStream.Flush();
                }
            }

            request.Abort();
            this._AsynchResponse.Close();
            this._AsynchResponse = null;

            cacheFileSize = (File.Exists(this._cacheFile)) ? (new FileInfo(this._cacheFile)).Length : 0;
            while (cacheFileSize != this._remoteFileSize)
            {
                cacheFileSize = (File.Exists(this._cacheFile)) ? (new FileInfo(this._cacheFile)).Length : 0;
                yield return null;
            }
#endif

            if (MD5Hashing.GetFileHash(this._cacheFile) == this._md5 || string.IsNullOrEmpty(this._md5 ))
            {
                try
                {
                    if (File.Exists(this._localFile)) { File.Delete(this._localFile); }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("<color=red>Could not delete local file</color>");
                    Debug.LogError(e);
                }

                while (File.Exists(this._localFile)) { yield return null; }

                File.Copy(this._cacheFile, this._localFile);

                localFileSize = (File.Exists(this._localFile)) ? (new FileInfo(this._localFile)).Length : 0;
                while (cacheFileSize != _remoteFileSize)
                {
                    localFileSize = (File.Exists(this._localFile)) ? (new FileInfo(this._localFile)).Length : 0;
                    yield return null;
                }

                try
                {
                    if (File.Exists(this._cacheFile)) { File.Delete(this._cacheFile); }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("<color=red>Could not delete cache file</color>");
                    Debug.LogError(e);
                }
                while (File.Exists(this._cacheFile)) { yield return null; }
            }
            else
            {
                Debug.LogError("ERROR: Integrity check failed (CHECKSUM EXPECTED: " + this._md5 + " BUT GOT " + MD5Hashing.GetFileHash(this._cacheFile) + ")");
                try
                {
                    if (File.Exists(this._cacheFile)) { File.Delete(this._cacheFile); }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("<color=red>Could not delete cache file</color>");
                    Debug.LogError(e);
                }
                while (File.Exists(this._cacheFile)) { yield return null; }
            }

            _isRunning = false;
            if(this.onLoadCompleted != null)
            {
                this.onLoadCompleted();
            }
        }

#if UNITY_WEBPLAYER || UNITY_WEBGL
	    private void DownloadCompleted(System.Object sender, AsyncCompletedEventArgs e) 
        {
		    this._isDone = true;
	    }
#else
        // Throwind an exception here will not propogate to unity!
        private void AsynchCallback(IAsyncResult result)
        {
            if (result == null) { Debug.LogError("Asynch result is null!"); }
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            if (webRequest == null) { Debug.LogError("Could not cast to web request"); }
            this._AsynchResponse = webRequest.EndGetResponse(result) as HttpWebResponse;
            if (this._AsynchResponse == null) { Debug.LogError("Asynch response is null!"); }
            Debug.Log("[ResumableDownload] Download compleate");
        }
#endif

        public string AssetName { get { return this._assetName; } }
    }
}