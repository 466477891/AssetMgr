﻿namespace Summoner
{
    using System.IO;
    using UnityEngine;
    using UnityEditor;
    public class ResourcesBuilder
    {
        [MenuItem("Apple/Build Resource", false, 52)]
        private static void BuildResourceFolder()
        {
            if (Directory.Exists(AssetPath.ResourcesPath))
            {
                Directory.Delete(AssetPath.ResourcesPath, true);
            }
            Directory.CreateDirectory(AssetPath.ResourcesPath);
            AssetVessel vessel = AssetDatabase.LoadAssetAtPath(AssetPath.AssetVesselPath, typeof(AssetVessel)) as AssetVessel;
            if(vessel != null)
            {
                foreach (AssetInfo info in vessel.assetList)
                {
                    string source = info.pathInEditor;
                    string destination = Path.Combine(AssetPath.ResourcesPath, Path.GetFileName(info.pathInEditor));
                    if (!File.Exists(destination))
                    {
                        File.Copy(source, destination);
                    }
                }
                GameConfigInfo information = new GameConfigInfo() { version = PlayerSettings.bundleVersion, platform = Application.platform, language = SystemLanguage.Chinese, assets = vessel.assetList };
                JsonSerializer.PackJsonToFile(AssetPath.ConfigPath, information, AssetPath.AssetConfigName.ToLower());
            }
        }
    }
}