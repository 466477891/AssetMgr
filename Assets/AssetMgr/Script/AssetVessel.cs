﻿namespace Summoner
{
    using UnityEngine;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    [System.Serializable]
    public class AssetVessel : ScriptableObject
    {
        [SerializeField]
        public List<AssetInfo> assetList = new List<AssetInfo>();
        [SerializeField]
        public List<RepeatAsset> repeatList = new List<RepeatAsset>();
        public string version = "0.1";
    }

    [System.Serializable]
    public class RepeatAsset
    {
        public string assetName = "";
        public List<AssetInfo> repeats = new List<AssetInfo>();
    }

    [System.Serializable]
    public class AssetInfo
    {
#if UNITY_EDITOR
        [JsonIgnore]
        public bool isUnfolder = false;
#endif
        public string assetName = string.Empty;
        public string type = string.Empty;
        public string Name = string.Empty;
        public int RamSize = 0;
        public int FileSize = 0;
        [JsonIgnore]
        public Object asset = null;
        public string pathInEditor = string.Empty;
        public string Md5 = string.Empty;
        public AssetInfo() { }
    }

    [System.Serializable]
    public class GameConfigInfo
    {
        public string version = "0.1";
        public RuntimePlatform platform = RuntimePlatform.Android;
        public SystemLanguage language = SystemLanguage.Chinese;
        public DefinitionLv definition = DefinitionLv.HD;
        public List<AssetInfo> assets = null;
    }

    public enum DefinitionLv
    {
        SD = 0,
        HD = 1,
        VHD = 2,
    }
}