﻿namespace Summoner
{
    using System.IO;
    using UnityEngine;

    public class AssetPath
    {
        private static string _loadablepath = string.Empty;
        private static string _readablepath = string.Empty;
        private static string _buildpath = string.Empty;
        private static string _netpath = "http://192.168.7.101:7888";//"192.168.8.177:4321";
        private static string _updatepath = "http://127.0.0.1:8080";
        /// <summary>
        /// true for using resource
        /// false for assetbundle
        /// </summary>
		public static bool UseResources = false;
        public static bool IsEditor
#if UNITY_EDITOR
        = Application.isEditor;//!for use assetbundle in editor mode
#else
        = Application.isEditor;
#endif

        //Get Load path
        public static string LoadAsyncPath
        {
            get
            {
#if UNITY_EDITOR
                _loadablepath = "file://" + Application.dataPath + "/StreamingAssets";
#elif UNITY_ANDROID
                _loadablepath = "jar:file://" + Application.dataPath + "!/assets";
#elif UNITY_IPHONE
				_loadablepath = "file://" + Application.dataPath + "/Raw";
#elif UNITY_WEBPLAYER
                _loadablepath = Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/")+ "/StreamingAssets";
#else
                _loadablepath = "file://" + Application.streamingAssetsPath;
#endif
                return _loadablepath;
            }
        }

        public static string LoadSyncPath
        {
            get
            {
#if UNITY_EDITOR
                _loadablepath = Application.dataPath + "/StreamingAssets";
#elif UNITY_ANDROID
                _loadablepath = Application.dataPath + "!assets";
#elif UNITY_IPHONE
				_loadablepath = Application.streamingAssetsPath;
#elif UNITY_WEBPLAYER
                _loadablepath = Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/")+ "/StreamingAssets";
#else
                _loadablepath = Application.streamingAssetsPath;
#endif
                return _loadablepath;
            }
        }

        //Get Write Path
        public static string DocumentsPath
        {
            get
            {
#if UNITY_IPHONE
                //string _writepath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/'));
                //_readablepath = _writepath.Substring(0, _writepath.LastIndexOf('/')) + "/Documents/";
                _readablepath = Application.persistentDataPath + "/Documents";
#elif UNITY_ANDROID
                _readablepath = Application.persistentDataPath + "/Documents";
#else
                _readablepath = Application.persistentDataPath + "/Documents";
#endif
                return _readablepath;
            }
        }

        public static string TemporaryCachePath { get { return Application.temporaryCachePath; } }


        /// <summary>
        /// Server side file path
        /// </summary>
        public static string NetPath
        {
            get { return _netpath; }
            set { _netpath = value; }
        }

        /// <summary>
        /// assets update server path
        /// </summary>
        public static string UpdatePath
        {
            get { return _updatepath; }
            set { _updatepath = value; }
        }
        public static string AssetConfigName { get { return "AssetInfos"; } }
        public static string StreamFolderName { get { return "StreamingAssets"; } }
        public static string UIConfigName { get { return "UIConfigInfo"; } }

        public static string PublishPath { get { return Path.Combine(Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/')), StreamFolderName).Replace("\\", "/"); } }
        public static string ConfigPath { get { return Path.Combine(Application.dataPath, "AssetConfig").Replace("\\", "/"); } }
        public static string ResourcesPath { get { return Path.Combine(Application.dataPath, "Resources").Replace("\\", "/"); } }
        public static string AssetVesselPath { get { return "Assets/AssetConfig/AssetVessel.asset"; } }
        public static string CachePath { get { return Path.Combine(Application.dataPath, "CacheAssets").Replace("\\", "/"); } }
        public static string StreamingAssetsPath { get { return Application.streamingAssetsPath; } }
    }
}