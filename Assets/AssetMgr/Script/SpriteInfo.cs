﻿namespace Summoner
{
    using System;
    using UnityEngine;

    [Serializable]
    public class SpriteInfo
    {
        public string name { get; set; }
        public Rect rect { get; set; }
        public Vector2 pivot { get; set; }
        public float pixelsPerUnit { get; set; }
        public Vector4 border { get; set; }
    }
}