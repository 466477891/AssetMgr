﻿namespace Summoner
{
    using UnityEngine;

    public class DestroyTip : MonoBehaviour
    {
        public string AssetName { get; set; }

        private bool _pooled = false;
        private bool _canDestoryed = false;

        public bool Pooled
        {
            get { return this._pooled; }
            set { this._pooled = value; }
        }

        public bool CanDestoryed
        {
            get { return this._canDestoryed; }
            set { this._canDestoryed = value; }
        }

        private void OnDestroy()
        {
            if(!this.CanDestoryed)
            {
                if(!this.Pooled)
                {
                    ULogger.LogWarning(string.Format("Can not destory this gameObject {0} yourself !", this.gameObject.name));
                }
                else
                {
                    AssetManager.DeleteAsset(this.gameObject);
                }
            }
        }
    }
}