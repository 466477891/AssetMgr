﻿namespace Summoner
{
    using UnityEngine;
    using System.Collections;
    using System;

    public class UpdateTest : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
            UpdateFeature.TryCheckUpdate(this.OnUpdated);
        }

        private void OnUpdated()
        {
            Debug.Log("Update completed~");
        }
    }
}