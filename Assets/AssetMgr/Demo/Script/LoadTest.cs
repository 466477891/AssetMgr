﻿namespace Summoner
{
    using UnityEngine;
    using System.Collections.Generic;

    public class LoadTest : MonoBehaviour
    {
        private string[] _assetnames = new string[] { "Capsule", "Cube", "Cylinder", "Sphere" };
        private List<Object> _curList = new List<Object>();
        private Texture2D _texture = null;
        private string _text = "";
        private bool _isInit = false;

        private void Awake()
        {
            AssetManager.Init(()=>
            {
                this._isInit = true;
            });
        }

        private void LoadAsync()
        {
            foreach (string str in _assetnames)
            {
                AssetManager.LoadGameObjectAsync(str, this.OnAssetLoaded);
            }
        }

        private void OnAssetLoaded(object obj)
        {
            GameObject go = (GameObject)obj;
            go.transform.position = Random.insideUnitSphere;
            this._curList.Add(go);
            Debug.Log(go.name + " has loaded ~");
        }

        private void LoadSync()
        {
            foreach (string str in _assetnames)
            {
                GameObject go = (GameObject)AssetManager.LoadGameObjectSync(str);
                go.transform.position = Random.insideUnitSphere;
                this._curList.Add(go);
            }
        }

        private void Clear()
        {
            foreach (GameObject obj in this._curList)
            {
                AssetManager.DeleteAsset(obj);
            }
            this._curList.Clear();
        }

        void OnGUI()
        {
            if (!this._isInit) { return; }
            if (GUILayout.Button("LoadAsync"))
            {
                this.LoadAsync();
            }
            if (GUILayout.Button("LoadSync"))
            {
                this.LoadSync();
            }
            if (GUILayout.Button("Clear"))
            {
                if (this._curList.Count > 0) { this.Clear(); }
            }
            if (GUILayout.Button("LoadTextureAsync"))
            {
                AssetManager.LoadTextureAsync("Character Pink Girl", (obj) =>
                {
                    this._texture = obj;
                });
            }
            if (GUILayout.Button("LoadTextureSync"))
            {
                this._texture = (Texture2D)AssetManager.LoadTextureSync("Character Pink Girl");
            }
            if (GUILayout.Button("DeleteTexture"))
            {
                if (this._texture != null)
                {
                    AssetManager.DeleteAsset(this._texture);
                    this._texture = null;
                }
            }
            if (GUILayout.Button("FetchSpriteAsync"))
            {
                AssetManager.FetchSpriteAsync("UIElements", "player", (obj) =>
                {
                    this._texture = obj.texture;
                });
            }
            if (GUILayout.Button("FetchSpriteSync"))
            {
                this._texture = AssetManager.FetchSpriteSync("UIElements", "btnA").texture;
            }
            if (GUILayout.Button("DeleteSprite"))
            {
                if (this._texture != null)
                {
                    this._texture = null;
                }
            }
            if (GUILayout.Button("LoadConfigSync"))
            {
                AssetManager.LoadConfigAsync("txttest", (content) =>
                {
                    this._text = content;
                });
            }
            if (GUILayout.Button("LoadConfigSync"))
            {
                this._text = AssetManager.LoadConfigSync("txttest");
            }
            if (GUILayout.Button("ClearConfig"))
            {
                this._text = "";
            }
            if (this._texture != null)
            {
                GUILayout.Label(this._texture);
            }
            if (!string.IsNullOrEmpty(this._text))
            {
                GUILayout.Label(this._text);
            }
        }

        void OnApplicationQuit()
        {
            foreach (GameObject go in this._curList)
            {
                AssetManager.DeleteAsset(go);
            }
            AssetManager.Instance.Dispose();
        }
    }
}